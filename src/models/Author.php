<?php

class Author
{
  const STATUS_ACTIVE = 'active';
  const STATUS_INACTIVE = 'inactive';

  const ROLE_COMMON = 'common';
  const ROLE_EXTERT = 'expert';

  private $id;
  private $name;
  private $email;
  private $status;
  private $role;
  private $created_at;
  private $updated_at;

  public function tableName()
  {
    return '{{author}}';
  }

  public function primaryKey()
  {
    return 'id';
  }

  public function rules()
  {
    return array(
      array('name, email', 'required'),
      array('status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE]),
      array('name', 'length', 'max' => 128),
      array('name', 'length', 'min' => 2),
    );
  }

  public function realtions()
  {
    return array(
      array('posts' => array(self::HAS_MANY, 'Post', 'author_id'))
    );
  }
}
