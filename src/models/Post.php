<?php

class Post
{
  const STATUS_ACTIVE = 'active';
  const STATUS_INACTIVE = 'inactive';

  private $id;
  private $title;
  private $body;
  private $status;
  private $author_id;
  private $created_at;
  private $updated_at;
  private $author;

  public function tableName()
  {
    return '{{posts}}';
  }

  public function primaryKey()
  {
    return 'id';
  }

  public function rules()
  {
    return array(
      array('title, body', 'required'),
      array('status', 'in', 'range' => [self::STATUS_PUBLISHED, self::STATUS_DRAFT]),
      array('title', 'length', 'min' => 16),
    );
  }

  public function realtions()
  {
    return array(
      'author' => array(self::BELONGS_TO, 'Author', 'author_id')
      /** @todo Добавить привязку к объекту Tag. 
       * Один к одному посту может быть привязано множество тегов.
       * Привязка возвращает только активные теги
       */
    );
  }

  public function scopes()
  {
    $alias = $this->getTableAlias(false, false);
    return array(
      'recent' => array(
        'order' => "{$alias}.id DESC"
      ),
      'active' => array(
        'condition' => "{$alias}.status = :status",
        'params' => ['status' => self::STATUS_ACTIVE]
      )
    );
  }

  /**
   * @todo Добавить 2 Named Scope для выбора постов по автору и по тегу
   */
}
